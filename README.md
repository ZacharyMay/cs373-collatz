# CS373: Software Engineering Collatz Repo

* Name: Zachary May

* EID: zsm334

* GitLab ID: ZacharyMay

* HackerRank ID: Euler72

* Git SHA: cda0ba27c6d30dcacfc94c6dbf5b06c42d66f870

* GitLab Pipelines: https://gitlab.com/ZacharyMay/cs373-collatz/pipelines

* Estimated completion time: 10

* Actual completion time: current time: 8 hours

* Comments: Good way to learn GitLab!
